# parez

The Cigydd’s parez LaTeX package

Includes styles used to typeset Ancient Hebrew and Mishay languages, 
along with personal LaTeX utilities.

## Table of Contents:
- parez.sty—a collection of custom macros
- mishayic.sty—used to typeset the Mishay language in a slighty modified Latin alphabet
- syntprep.sty—used for syntactical transcription of Ancient Hebrew
- shebrew.sty—used to typeset Hebrew using the Shebrew TrueType font (for those who got it legally)
- bwhebrew.sty—used to typeset Hebrew using the BibleWorks Bwhebl & Bwhebb fonts 
    (for those who purchased the BibleWorks software legally; see https://www.bibleworks.com/)
- unicodehebrew.sty—used to typeset Hebrew with TrueType fonts implementing Unicode and the Hebrew script
- cwiczebnice.cls—a class based on the standard LaTeX book class dedicated for typesetting the Hebrew Workbook
    /Cvičebnice biblické hebrejštiny/ written in Czech by /Martin Prudký/; this was a plan that didn't go any
    further than a couple of pages because the autor was faster than me with his reedition
- kniha.cls—used for typesetting a religion science textbook /Nástin religionistiky/ written in Czech
    by /Jan Heller/ and /Milan Mrázek./ A work in progress, still finding a path for publishing this edition
- parezverbatim.sty—an attempt to create a custom verbatim environment
- README.md—this file

## Installation
The *.cls and *.sty files you want to use should be copied to directory /tex/latex/parez in your texmf tree 
($HOMETEXMF is fine).
